package storages

import (
	"gitlab.com/a3buka/restlike/user/internal/db/adapter"
	"gitlab.com/a3buka/restlike/user/internal/infrastructure/cache"
	//vstorage "gitlab.com/a3buka/restlike/user/internal/modules/auth/storage"
	ustorage "gitlab.com/a3buka/restlike/user/internal/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
	//Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
		//Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
