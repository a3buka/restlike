package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/a3buka/restlike/user/internal/infrastructure/component"
	"gitlab.com/a3buka/restlike/user/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	userController := controllers.User
	r.Route("/", func(r chi.Router) {
		r.Post("/changePassword", userController.ChangePassword)
		//r.Get("/getById", userController.GetById)
		r.Get("/profile/{id}", userController.Profile)
		r.Post("/create", userController.Create)
		r.Get("/email/{email}", userController.GetByEmail)
	})
	return r
}
