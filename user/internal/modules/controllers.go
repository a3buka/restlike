package modules

import (
	"gitlab.com/a3buka/restlike/user/internal/infrastructure/component"
	ucontroller "gitlab.com/a3buka/restlike/user/internal/modules/user/controller"
)

type Controllers struct {
	//Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	//authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		//Auth: authController,
		User: userController,
	}
}
