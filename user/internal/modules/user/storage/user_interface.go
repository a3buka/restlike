package storage

import (
	"context"
	"gitlab.com/a3buka/restlike/user/internal/db/adapter"
	"gitlab.com/a3buka/restlike/user/internal/models"
)

type Userer interface {
	Create(ctx context.Context, u models.UserDTO) (int, error)
	Update(ctx context.Context, u models.UserDTO) error
	GetByID(ctx context.Context, id int) (models.UserDTO, error)
	GetByIDs(ctx context.Context, ids []int) ([]models.UserDTO, error)
	GetByEmail(ctx context.Context, email string) (models.UserDTO, error)
	GetByFilter(ctx context.Context, condition adapter.Condition) ([]models.UserDTO, error)
}
