package controller

import (
	"fmt"
	"github.com/ptflp/godecoder"
	"gitlab.com/a3buka/restlike/user/internal/infrastructure/component"
	"gitlab.com/a3buka/restlike/user/internal/infrastructure/errors"
	"gitlab.com/a3buka/restlike/user/internal/infrastructure/responder"
	"gitlab.com/a3buka/restlike/user/internal/modules/user/service"
	"net/http"
	"strconv"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	Create(w http.ResponseWriter, r *http.Request)
	ChangePassword(w http.ResponseWriter, r *http.Request)
	//GetById(w http.ResponseWriter, r *http.Request)
	GetByEmail(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

//func(u *User) GetById(w http.ResponseWriter, r *http.Request){
//	claims, err := handlers.ExtractUser(r)
//	if err != nil {
//		u.ErrorBadRequest(w, err)
//		return
//	}
//	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
//	if out.ErrorCode != errors.NoError {
//		u.OutputJSON(w, ProfileResponse{
//			ErrorCode: out.ErrorCode,
//			Data: Data{
//				Message: "retrieving user error",
//			},
//		})
//		return
//	}
//
//	u.OutputJSON(w, ProfileResponse{
//		Success:   true,
//		ErrorCode: out.ErrorCode,
//		Data: Data{
//			User: *out.User,
//		},
//	})
//}

func (u *User) GetByEmail(w http.ResponseWriter, r *http.Request) {
	email := r.Header.Get("email")
	userOut := u.service.GetByEmail(r.Context(), service.GetByEmailIn{Email: email})
	if userOut.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: userOut.ErrorCode,
			Data: Data{
				Message: "user GerEmail mistake",
			},
		})
		return
	}

}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.Header.Get("id"))
	if err != nil {
		u.OutputJSON(w, "invaild tip id")
	}

	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: id})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (u *User) Create(w http.ResponseWriter, r *http.Request) {
	var userCreateIn service.UserCreateIn
	err := u.Decode(r.Body, &userCreateIn)
	if err != nil {
		u.ErrorBadRequest(w, err)
	}
	userCreateOut := u.service.Create(r.Context(), userCreateIn)
	if userCreateOut.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: userCreateOut.ErrorCode,
			Data: Data{
				Message: "not create"},
		})
		return
	}
	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: userCreateOut.ErrorCode,
		Data: Data{
			Message: fmt.Sprintf("user %d, succsessfully created", userCreateOut.UserID),
		},
	})
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var changePasswordId service.ChangePasswordIn
	err := u.Decode(r.Body, &changePasswordId)
	if err != nil {
		u.ErrorBadRequest(w, err)
	}
	if u.service != nil {
		fmt.Println(u.service)
	}
	changePasswordOut := u.service.ChangePassword(r.Context(), changePasswordId)
	if changePasswordOut.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: changePasswordOut.ErrorCode,
			Data: Data{
				Message: "not update pass"},
		})
		return
	}
	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: changePasswordOut.ErrorCode,
		Data: Data{
			Message: "successfully update",
		},
	})
}
