package modules

import (
	"gitlab.com/a3buka/restlike/user/internal/infrastructure/component"
	//aservice "gitlab.com/a3buka/restlike/user/internal/modules/auth/service"
	uservice "gitlab.com/a3buka/restlike/user/internal/modules/user/service"
	"gitlab.com/a3buka/restlike/user/internal/storages"
)

type Services struct {
	User uservice.Userer
	//Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		//Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
