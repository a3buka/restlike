package docs

import (
	ucontroller "gitlab.com/a3buka/restlike/auth/internal/modules/user/controller"
	"gitlab.com/a3buka/restlike/auth/internal/modules/user/service"
)

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/user/profile user profileRequestRequest
// Получение информации о текущем профиле.
// security:
//	 - Bearer: []
// responses:
//   200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}

// swagger:route PUT /api/1/user/profile/changePassword user changePasswordRequest
// Изменение пароля текущего пользователя.
// security:
//	 - Bearer: []
// responses:
//   200: changePasswordResponse

// swagger:parameters changePasswordRequest
type changePasswordRequest struct {
	// in:body
	Body service.ChangePasswordIn
}

// swagger:response changePasswordResponse
type changePasswordResponse struct {
	// in:body
	Bode service.ChangePasswordOut
}
