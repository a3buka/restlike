package modules

import (
	"gitlab.com/a3buka/restlike/auth/internal/infrastructure/component"
	acontroller "gitlab.com/a3buka/restlike/auth/internal/modules/auth/controller"
	"gitlab.com/a3buka/restlike/auth/internal/modules/user/controller"
	//ucontroller "gitlab.com/a3buka/restlike/user/internal/modules/user/controller"
	//controller "gitlab.com/a3buka/restlike/user/internal/modules/user/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	User controller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := controller.NewUser(components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
