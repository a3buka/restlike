package controller

import (
	"fmt"
	"github.com/ptflp/godecoder"
	"gitlab.com/a3buka/restlike/auth/internal/infrastructure/component"
	"gitlab.com/a3buka/restlike/auth/internal/models"

	"gitlab.com/a3buka/restlike/auth/internal/infrastructure/errors"
	"gitlab.com/a3buka/restlike/auth/internal/infrastructure/handlers"
	"gitlab.com/a3buka/restlike/auth/internal/infrastructure/responder"

	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	Create(w http.ResponseWriter, r *http.Request)
	ChangePassword(w http.ResponseWriter, r *http.Request)
}

type User struct {
	responder.Responder
	godecoder.Decoder
}

func NewUser(components *component.Components) Userer {
	return &User{Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	resp, err := http.Get(fmt.Sprintf("http://127.0.0.1:8081/profile/{%d}", claims.ID))
	if err != nil {
		u.OutputJSON(w, "mistake microservice")

	}
	type UserOut struct {
		User      *models.User `json:"user"`
		ErrorCode int          `json:"error_code"`
	}
	var userOut = UserOut{}

	u.Decode(resp.Body, &userOut)
	if userOut.ErrorCode != errors.NoError {

		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: userOut.ErrorCode,
		Data: Data{
			User: *userOut.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (u *User) Create(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	//resp, _ := http.Get("localhost:8081/api/1/user/profile/changePassword")
	//resp.Body

	resp, err := http.Post("http://127.0.0.1:8081/changePassword", "application/json", r.Body)
	//resp. err := http.P
	if err != nil {
		u.ErrorBadRequest(w, err)
	}
	type ChangePasswordOut struct {
		Success   bool `json:"success"`
		ErrorCode int  `json:"error_code"`
	}
	var changePasswordOut ChangePasswordOut
	err = u.Decode(resp.Body, &changePasswordOut)
	if err != nil {
		u.ErrorBadRequest(w, err)
	}
	defer resp.Body.Close()
	if changePasswordOut.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: changePasswordOut.ErrorCode,
			Data: Data{
				Message: "not update pass"},
		})
		return
	}
	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: changePasswordOut.ErrorCode,
		Data: Data{
			Message: "successfully update",
		},
	})
}
